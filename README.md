# Eggrty-Utils

## Guide

全局安装即可.

```bash
npm install git+https://gitlab.com/eggrty/eggrty-utils.git
```

## Getting Start

## 注意事项

* 迭代时要维护好 unit test
* 提交代码后要注意 CI 平台、邮件是否有单测失败反馈
